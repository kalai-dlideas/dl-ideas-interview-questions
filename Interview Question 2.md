
## Interview Question 


Write a simple PHP/JS class which displays an introductory message like 
>"Hello DL Ideas. Nice to meet you",
 
where `DL Ideas` is an argument value of the method within the class.


Use one of these framework
`Laravel`, `CodeIgniter`, `Node.js` or `Express.js`

### Note
1. Once completed, make sure your repo is public and share us the link.
2. Make sure you with PC/Notebook etc to explain your code if required during interview session.