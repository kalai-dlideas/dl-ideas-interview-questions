## Interview Test
>You are required to develop in `Laravel` or `CodeIgniter`.


## Assume data below provided

Users: [https://jsonplaceholder.typicode.com/users](https://jsonplaceholder.typicode.com/users)

Todo:[https://jsonplaceholder.typicode.com/todos](https://jsonplaceholder.typicode.com/todos) 

1. Return list of Top Completed Todo user order by number of todo. (display in Data Table)
2. Search REST API - Create an endpoint that allow user to filter the Todo base on all the available fields. Your solution needs to be scalable.

### Note
1. Once completed, make sure your repo is public and share us the link.
2. Make sure you with PC/Notebook etc to explain your code if required during interview session.

