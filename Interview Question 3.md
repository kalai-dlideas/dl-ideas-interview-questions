
## Interview Question 

Create a system which allows users to create and manage their  task lists.

1. Each task list must be assigned to a category
>Example: 'Hobby', 'Important',  'Remind me later'.

- Able to View task & categories
- Able to Create Task & categories
- Able to Delete Task & categories
- Able to Edit Task & categories

Feel free to design it however you want it to be. 
You should use `MySQL` or `PostgreSQL` to store the data in the database.


### Note
1. Once completed, make sure your repo is public and share us the link.
2. Make sure you with PC/Notebook etc to explain your code if required during interview session.